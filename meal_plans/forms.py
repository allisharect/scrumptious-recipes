from django import forms

from meal_plans.models import MealPlan, Rating


class MealPlanForm(forms.ModelForm):
    class Meta:
        model = MealPlan
        fields = [
            "name",
        ]


class RatingForm(forms.ModelForm):
    class Meta:
        model = Rating
        fields = ["value"]
