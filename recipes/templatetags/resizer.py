from ctypes import resize
from django import template

from recipes.models import Ingredient

register = template.Library()


def resize_to(ingredient, target):

    num_servings = ingredient.recipe.servings
    if num_servings != None and target != None:
        try:
            ratio = int(target) / num_servings
            return ratio * ingredient.amount
        except ValueError:
            pass
    return ingredient.amount
    # Get the number of servings from the ingredient's
    # recipe using the ingredient.recipe.servings
    # properties

    # if the servings from the recipe is not None
    #   and the value of target is not None
    # try
    # calculate the ratio of target over
    #   servings
    # return the ratio multiplied by the
    #   ingredient's amount
    # catch a possible error
    # pass
    # return the original ingredient's amount since
    #   nothing else worked


register.filter(resize_to)
