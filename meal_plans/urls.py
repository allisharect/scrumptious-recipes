from django.urls import path

from meal_plans.views import (
    MealPlanCreateView,
    MealPlanUpdateView,
    MealPlanDeleteView,
    MealPlanDetailView,
    MealPlanListView,
    log_rating,
)


urlpatterns = [
    path("meal_plans/", MealPlanListView.as_view(), name="meal_plans_list"),
    path(
        "meal_plans/<int:pk>/",
        MealPlanDetailView.as_view(),
        name="meal_plans_detail",
    ),
    path(
        "meal_plans/create/",
        MealPlanCreateView.as_view(),
        name="meal_plans_new",
    ),
    path(
        "meal_plans/<int:pk>/edit/",
        MealPlanUpdateView.as_view(),
        name="meal_plans_edit",
    ),
    path(
        "meal_plans/<int:pk>/delete/",
        MealPlanDeleteView.as_view(),
        name="meal_plans_delete",
    ),
    path("<int:mealplan_id>/ratings/", log_rating, name="meal_plans_rating"),
]
