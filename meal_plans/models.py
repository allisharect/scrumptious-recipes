from django.conf import settings
from django.db import models
from django.forms import CharField, DateField
from django.core.validators import MaxValueValidator, MinValueValidator

from recipes.models import Recipe

from recipes.models import USER_MODEL

USER_MODEL = settings.AUTH_USER_MODEL

# Create your models here.


class MealPlan(models.Model):
    name = models.CharField(max_length=120)
    created = models.DateField(auto_now_add=True)
    updated = models.DateField(auto_now_add=True)
    owner = models.ForeignKey(USER_MODEL, on_delete=models.CASCADE, null=True)
    recipes = models.ManyToManyField("recipes.Recipe", related_name="recipes")

    def __str__(self):
        return str(self.name)


class Rating(models.Model):
    value = models.PositiveSmallIntegerField(
        validators=[
            MaxValueValidator(5),
            MinValueValidator(1),
        ]
    )
    mealplan = models.ForeignKey(
        "MealPlan",
        related_name="ratings",
        on_delete=models.CASCADE,
    )
