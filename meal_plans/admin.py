from django.contrib import admin

# Register your models here.
from meal_plans.models import MealPlan, Rating


class MealPlanAdmin(admin.ModelAdmin):
    pass


class RatingAdmin(admin.ModelAdmin):
    pass


# Register your models here.
admin.site.register(MealPlan, MealPlanAdmin)
admin.site.register(Rating, RatingAdmin)
